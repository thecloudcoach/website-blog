<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><title>Terraform &amp; Ansible Pull - The Cloud Coach</title><meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:title" content="Terraform &amp; Ansible Pull" />
<meta property="og:description" content="Combine Ansible (Pull) and Terraform for unlimited scalability." />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://thecloud.coach/posts/terraform-ansible-pull/" />
<meta property="article:published_time" content="2019-07-27T00:00:00+00:00" />
<meta property="article:modified_time" content="2019-07-27T00:00:00+00:00" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:title" content="Terraform &amp; Ansible Pull"/>
<meta name="twitter:description" content="Combine Ansible (Pull) and Terraform for unlimited scalability."/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,300italic,400italic|Raleway:200,300" rel="stylesheet">

	<link rel="stylesheet" type="text/css" media="screen" href="https://thecloud.coach/css/normalize.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="https://thecloud.coach/css/main.css" />

	
	<script src="https://thecloud.coach/js/main.js"></script>
</head>

<body>
	<div class="container wrapper post">
		<div class="header">
	<h1 class="site-title"><a href="https://thecloud.coach/">The Cloud Coach</a></h1>
	<div class="site-description"><h2>CloudOps Educational Resources</h2><nav class="nav social">
			<ul class="flat"></ul>
		</nav>
	</div>

	<nav class="nav">
		<ul class="flat">
			
			<li>
				<a href="https://store.thecloud.coach/">Courses</a>
			</li>
			
			<li>
				<a href="https://discord.gg/MTzBvSS">Discord</a>
			</li>
			
		</ul>
	</nav>
</div>


		<div class="post-header">
			<h1 class="title">Terraform &amp; Ansible Pull</h1>
			<div class="meta">Posted at &mdash; Jul 27, 2019</div>
		</div>

		<div class="markdown">
			<p>I was first introduced to Configuration Management via <a href="https://puppet.com/product">Puppet</a>. I used
it extensively at several engagements and it did the job rather well. The DSL was workable, writing
modules was simple enough, and Puppet its self, as well as the projects around it
(<a href="https://www.theforeman.org">Foreman</a> and <a href="https://docs.puppet.com/hiera/">Hiera</a>) did a great job
of making the whole state management process easier.</p>
<p>During that time I had also discovered and grew very fond of Ansible. It was quickly becoming the
superior product from my perspective, primarily due to the simple integration and push (by default)
based model. <strong>I haven’t touched Puppet for nearly four years.</strong></p>
<p>Although Ansible won out with its push model, time and time again I found managing inventories and
executing tasks locally tedious. I often caught my self wishing I could actually just implement some
centralised system that Ansible pulled from. I obviously knew of <code>ansible-pull</code>, but what would the
architecture look like? How could I still get Ansible&rsquo;s frictionless push model integration into a
network and still remove the need for dealing with inventories?</p>
<p>I essentially wanted to update the state, push it, and then have the network apply the new state.</p>
<h4 id="a-shell-script">A shell script</h4>
<p>After thinking about various solutions, which obviously involved a lot of overthinking, I eventually
settled on an extremely obvious, very simple but effective solution: a shell script executed on a
new instance via the user data/<a href="https://cloudinit.readthedocs.io/en/latest/">cloud-init</a> feature to
install Ansible and run <code>ansible-pull</code>.</p>
<p>It’s a five line shell script:</p>
<div class="highlight"><pre style="color:#93a1a1;background-color:#002b36;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-bash" data-lang="bash"><span style="color:#719e07">#!/bin/bash
</span><span style="color:#719e07"></span>yum update -y
yum install epel-release -y
yum install ansible git -y
ansible-pull provision.yml -U <span style="color:#2aa198">${</span><span style="color:#268bd2">git_repository</span><span style="color:#2aa198">}</span> -f --clean --accept-host-key -i localhost, -e <span style="color:#268bd2">git_respository</span><span style="color:#719e07">=</span><span style="color:#2aa198">${</span><span style="color:#268bd2">git_repository</span><span style="color:#2aa198">}</span> -e <span style="color:#268bd2">environment</span><span style="color:#719e07">=</span><span style="color:#2aa198">${</span><span style="color:#268bd2">environment</span><span style="color:#2aa198">}</span> -e <span style="color:#268bd2">role</span><span style="color:#719e07">=</span><span style="color:#2aa198">${</span><span style="color:#268bd2">system_role</span><span style="color:#2aa198">}</span>
</code></pre></div><p>It includes some variables in here. These variables are managed by
<a href="https://www.terraform.io">Terraform</a> as the script is a Terraform template:</p>
<div class="highlight"><pre style="color:#93a1a1;background-color:#002b36;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-fallback" data-lang="fallback">data &#34;template_file&#34; &#34;userdata&#34; {
	template = file(&#34;${path.module}/files/userdata.sh&#34;)
	vars {
		git_repository = var.git_repository
		environment = var.environment
		system_role = var.system_role
	}
}
</code></pre></div><p>It’s injected via a user data directive within Terraform, which allows for hands off deployments:</p>
<div class="highlight"><pre style="color:#93a1a1;background-color:#002b36;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-fallback" data-lang="fallback">resource &#34;aws_instance&#34; &#34;bounce&#34; {
	key_name = &#34;Secret Key&#34;
	ami = &#34;ami-fedafc9d&#34;
	instance_type = &#34;t2.nano&#34;
	vpc_security_group_ids = [aws_security_group.bounce.id]
	subnet_id = aws_subnet.bounce.id
	user_data = data.template_file.userdata.rendered
	tags {
		Name = &#34;bouncy-castle&#34;
	}
}
</code></pre></div><p>The sample here demonstrates a static instance being setup, but the process is exactly the same for
AWS Launch Configurations (to back an Auto Scaling Group) for dynamically scaling, volatile
instances.</p>
<p>Once a system is starting up and this whole process is put into action, two Playbooks then control
the present and future state of the new system: <code>provision.yaml</code> and <code>update.yaml</code>.</p>
<h4 id="provisioning">Provisioning</h4>
<p>The process of provisioning the system is the first stage to the management of the OS and above.
This stage gets the system ready to perform its role and in a stable condition.</p>
<h4 id="updating">Updating</h4>
<p>The process of updating the system in place is somewhat optional in this architecture. Some people
don’t like the idea of updating infrastructure in place, especially now with the ease of
implementing CI/CD processes; automation tools; testing; and so on. Feel free to omit this option.</p>
<h4 id="thats-the-basics">That’s the basics</h4>
<p>With the right amount of consideration, the <code>ansible-pull</code> model of working is rather quite simple,
easy to grasp and implement, and uses tools already present in the network (most likely, anyway.)</p>
<p>Like all solutions, however, it can’t be everything to everyone</p>

		</div>

		<div class="post-tags">
			
				
					<nav class="nav tags">
							<ul class="flat">
								
								<li><a href="/tags/terraform">terraform</a></li>
								
								<li><a href="/tags/ansible">ansible</a></li>
								
								<li><a href="/tags/automation">automation</a></li>
								
								<li><a href="/tags/aws">aws</a></li>
								
								<li><a href="/tags/devops">devops</a></li>
								
							</ul>
					</nav>
				
			
		</div>
		</div>
	<div class="footer wrapper">
	<nav class="nav">
		<div> <a href="https://github.com/vividvilla/ezhil">Ezhil theme</a> | Built with <a href="https://gohugo.io">Hugo</a></div>
	</nav>
</div>




</body>
</html>
