<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><title>Organising Terraform Workloads - The Cloud Coach</title><meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:title" content="Organising Terraform Workloads" />
<meta property="og:description" content="Use Terraform modules to seperate your design from your implementation to allow for perfectly identical clones every time." />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://thecloud.coach/posts/organising-terraform-workloads/" />
<meta property="article:published_time" content="2019-12-13T00:00:00+00:00" />
<meta property="article:modified_time" content="2019-12-13T00:00:00+00:00" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:title" content="Organising Terraform Workloads"/>
<meta name="twitter:description" content="Use Terraform modules to seperate your design from your implementation to allow for perfectly identical clones every time."/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,300italic,400italic|Raleway:200,300" rel="stylesheet">

	<link rel="stylesheet" type="text/css" media="screen" href="https://thecloud.coach/css/normalize.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="https://thecloud.coach/css/main.css" />

	
	<script src="https://thecloud.coach/js/main.js"></script>
</head>

<body>
	<div class="container wrapper post">
		<div class="header">
	<h1 class="site-title"><a href="https://thecloud.coach/">The Cloud Coach</a></h1>
	<div class="site-description"><h2>CloudOps Educational Resources</h2><nav class="nav social">
			<ul class="flat"></ul>
		</nav>
	</div>

	<nav class="nav">
		<ul class="flat">
			
			<li>
				<a href="https://store.thecloud.coach/">Courses</a>
			</li>
			
			<li>
				<a href="https://discord.gg/MTzBvSS">Discord</a>
			</li>
			
		</ul>
	</nav>
</div>


		<div class="post-header">
			<h1 class="title">Organising Terraform Workloads</h1>
			<div class="meta">Posted at &mdash; Dec 13, 2019</div>
		</div>

		<div class="markdown">
			<p>If you&rsquo;ve worked with Terraform on a large enough scale you&rsquo;ll agree that a single repository with a flat file hierarchy of code is difficult to manage, navigate and execute. It&rsquo;s also difficult to create multiple instances of infrastructure from a single code base, so you end up forking it, and the fragmentation/split brain begins.</p>
<p>But there is a better way.</p>
<hr>
<p>The primary problem I want to address and solve here is really simple to grasp: I want a single
directory of code and I can repeatedly deploy knowing its identical every time, without having to
create multiple copies of it.</p>
<p>For me all implementations (prod, nonprod, staging, uat, etc) of a design should be identical and
managed from a single piece of code. I believe the organisational method I outline below achieves
this.</p>
<h2 id="split-it-up">Split it up</h2>
<p>I&rsquo;ve found a good starting point for organising one&rsquo;s Terraform code is to keep the &ldquo;design&rdquo;
separated from the &ldquo;implementation&rdquo;. I also have a &ldquo;management&rdquo; tier to support each
&ldquo;implementation&rdquo;.</p>
<p>The directory structure might look something like this:</p>
<pre><code>.
|-- README.md
|-- .gitlab-ci.yml
|-- management
|   |-- gitlab-runner.tf
|   |-- outputs.tf
|   |-- providers.tf
|   |-- security_groups.tf
|   |-- state.tf
|   |-- subnets.tf
|   |-- terraform.tfvars
|   |-- inputs.tf
|   `-- vpc.tf
|-- production.tf
`-- design
    |-- main.tf
    |-- terraform.tfvars
    `-- variables.tf
</code></pre>
<p>Let me break all of that down a bit because frankly what I just said is confusing me, and I wrote
it&hellip;</p>
<h3 id="the-management-tier">The Management Tier</h3>
<p>This is potentially an optional thing to have. I&rsquo;ll explain what it means anyway and let you decide
if it&rsquo;s something you want to employ.</p>
<p>In most cases you&rsquo;re going to build out some infrastructure (as code) and then you&rsquo;ll (ideally) want
to implement a CI/CD stack to manage that code. The CI/CD solution needs to be stood up first before
the design can be deployed.</p>
<p>Depending on your goals or your design, you likely want two environments in the same VPC (but with
clear security boundaries), so the management tier stands up that VPC (and those security
boundaries) for you.</p>
<p>It&rsquo;s also going to standup S3 Buckets and the DynamoDB Tables for use as state storage and locking
mechanisms. Of course you might be using AzureRM or Terraform Cloud/Enterprise.</p>
<p>Essentially it&rsquo;s designed to stand up the &ldquo;meta&rdquo; infrastructure that manages and supports your
infrastructure. It means the design doesn&rsquo;t have to manage anything to do with networking outside of
firewall rules and simple routing decisions.</p>
<p>It&rsquo;s entirely optional, though. You might want your design to implement its own VPC because you want
a VPC per environment. That&rsquo;s fine too, and potentially a better idea depending on requirements.</p>
<h3 id="the-design">The Design</h3>
<p>When you have something to stand up in, say, AWS, you start architecting the solution on paper (or
in draw.io.) This includes the finer details like the VPC, subnets, EC2 Instances, EFS, S3 Buckets,
and more. It&rsquo;s the actual &ldquo;guts&rdquo; of a solution. That&rsquo;s a design.</p>
<p>That design should be represented as a module in Terraform. That module should not have a
<code>provider{}</code> or a <code>terraform{}</code> configuration. It should just be a module with inputs and outputs.
The entire thing: a module.</p>
<p>The module is then imported into an &ldquo;implementation&rdquo; that represents some environment. This module
is fed the outputs from the management tier (should you choose to the fully adopt this model) such
as subnet IDs, etc, and then it does its thing.</p>
<p>Where is the design module imported?</p>
<h3 id="one-design-many-identical-environments">One Design, Many (Identical) Environments</h3>
<p>At the environment level. This can be implemented in one of two ways.</p>
<p>The above output from <code>tree</code> shows a <code>production.tf</code> file. This imports the <code>./design</code> module. When
being imported the module can be fed information from the management tier, or not, about where it is
to stand up its resources. And from here on the idea behind using a module to represent a design
becomes apparant.</p>
<p>Next you might want a staging environment: <code>staging.tf</code>. You create that file, import the same
module, but provide different inputs such as different subnet IDs, and you&rsquo;ve got a guarenteed
identical setup to <code>production.tf</code> but in the staging part of the VPC.</p>
<p>But the above method assumes you&rsquo;re happy to keep each environment&rsquo;s state in a single state file
(regardless of where it&rsquo;s stored/managed.) This might not work for you or your organisation&rsquo;s
auditing and security models.</p>
<p>Personally I&rsquo;m OK with this because each environment is built off of the same design – they&rsquo;re
identical – so really there&rsquo;s nothing too individualistic about each environment exept their
placement within your remote environment.</p>
<p>Should it bother you, however, or not fit in with how you want to work, you can simply take
advantage of the fact Terraform lets us reference modules using relative paths.</p>
<p>Instead of root level files, we can create an <code>environments/</code> directory which contains
subdirectories for each environment. Inside of these we can now provide unique <code>terraform{}</code>
configurations per environment, allowing you to keep their state (and locking capabilities) separate
and isolated.</p>
<p>All you have to do now is reference the module as <code>../../workload</code> instead of <code>./workload</code>. That&rsquo;s
easy enough to manage.</p>
<h2 id="summary">Summary</h2>
<p>The idea is to keep the design representative of the architecture you want to implement, minus a few
details such as high level networking and security boundaries. This enables you to create multiple
instances of your workload knowing they&rsquo;re perfectly identical every time.</p>
<p>What are your thoughts? How do you manage your code base?</p>

		</div>

		<div class="post-tags">
			
				
					<nav class="nav tags">
							<ul class="flat">
								
								<li><a href="/tags/automation">automation</a></li>
								
								<li><a href="/tags/deployment">deployment</a></li>
								
								<li><a href="/tags/devops">devops</a></li>
								
								<li><a href="/tags/terraform">terraform</a></li>
								
							</ul>
					</nav>
				
			
		</div>
		</div>
	<div class="footer wrapper">
	<nav class="nav">
		<div> <a href="https://github.com/vividvilla/ezhil">Ezhil theme</a> | Built with <a href="https://gohugo.io">Hugo</a></div>
	</nav>
</div>




</body>
</html>
