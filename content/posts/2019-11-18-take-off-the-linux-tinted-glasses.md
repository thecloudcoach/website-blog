---
title: Take off the Linux Tinted Glasses
date_updated: 2020-02-07T08:46:28.000Z
tags: [operating-systems, freebsd, Getting Started]
draft: true
description: We must view new tools with a clear mind.
---

Recently I watched [a video on YouTube](https://www.youtube.com/watch?v=mgeHGVLsij4) about FreeBSD.
It was about a guy trying FreeBSD for the first time. He was attempting to assertain if FreeBSD
would act as a desktop alternative to Linux. I'm not going to disect the video here, but I do want
to talk about one issue with his entire approach to FreeBSD.

---

By the time I got to the end of the video I realised one big problem with the way he was approaching
FreeBSD: he was looking at it through the eyes of a Linux user expecting another Linux like
experience. At least that's how I interrupted the video.

I'd even go as far as to say subconsciously **he was expecting FreeBSD to be yet another Linux
distribution**. Not literally – I'm aware he understands the difference between Linux and FreeBSD –
but I'm willing to bet he's looking for a Linux alternative and wants FreeBSD to behave in a certain
way.

The problem is FreeBSD and other BSD variants are completely different solutions to the same problem
– for both desktop and server workloads. FreeBSD is designed in a different manner, maintained on a
different schedule, has a different philosophy, and  more. It's a similar solution, but just a
different tool.

Getting back to the video, I think this comment by [Donald
Mickunas](https://www.youtube.com/channel/UCzjwEiahe5XGXHgvIi5f_nA) sums it up nicely (you can see
my reply below his):

> First, the development branch for BSD is quite different than that for Linux.  So, assuming that
> what works on Linux will work on BSD is a mistake.BSD and its variants deserve to be taken
> seriously.  Consider the time it took to become comfortable with Linux. Please be wiling to take a
> comparable amount of time to get comfortable with BSD.In my experience, freeBSD is well documented
> and can be learned and used by anyone willing to put in the time and effort.FreedBSD is not Linux
> though both were originally based on Unix.

Spot on.

Perhaps the FreeBSD documentation should make it clearer that any BSD variant is not going to give
the end user a Linux like experience, especially on the desktop?
