---
title: "FreeBSD: is it a Linux alternative?"
slug: a-transition-to-freebsd
date_published: 2019-11-12T22:41:43.000Z
date_updated: 2020-02-07T08:46:42.000Z
tags: devops, freebsd, operating-systems
draft: true
excerpt: Is FreeBSD a better alternative to Linux? Can BSD Jails and ZFS replace Docker for me?
---

Recently I've decided to revisit FreeBSD as an alternative to Linux in the server/hosting space. It's been about ten years since I last used FreeBSD in any kind of serious way, but I do remember FreeBSD being an excellent, complete ecosystem with a foundation built on stability, predictability and security.

I've used Linux in the server space for decades now, and I'm wondering: am I missing out on an even better solution? In fact, what am I looking for?

### **Jails**

I'm not a fan of Docker and the container rush that's going on right now. I think containers have a place and solve a problem, but I think it's a problem not many people have. And despite all the goodness a single node K8s cluster gets you on your local desktop, I cannot believe people would favour a complex monolith they most definitely do not understand over simple files on a disk. But that's another rant for another post.

FreeBSD offers a much cleaner solution called Jails. I'm hoping Jails can deliver on the key principles that I need when running a web application:

- Process isolation
- Easy upgrade paths
- Templating/inheritance
- Portability
- Ability to manage from a CI/CD pipeline

Given my limited research so far I'm confident all of these things are simple for Jails to accomplish (with a little help from some utility apps.) And all of this comes from simple principles and is mostly handled using easy to read/parse flat files on disk and offline tools (as opposed to running an entire separate operating system on top of your operating system to manage a process.)

### **I Want a Cathedral, not a Bazaar**

I also want a well maintained, coherent solution for hosting networked services. I don't feel any Linux distribution really offers this as an operating system. Linux feels disjointed and cobbled together. Even the mainstream distributions don't really give me a sense of coherence.

I want something with:

- A solid package manager offering both binaries and source compilation (for maximum control when needed);
- Base tools that are tightly integrated into the overall system, and maintained as one large ecosystem;
- Sensible defaults and configurability, with files being in a fixed, sensible order and location;
- A solid upgrade path between versions – one that's safe;
- Advanced file systems;
- Snapshots and backup capabilities built in;

I'm not certain there is a Linux distribution out there that could fulfill these requirements in a way I'd be happy with. Arch Linux might come close, and I know for sure Gentoo can offer both binary and source packages.

But FreeBSD gives me all of these things out of the box too whilst also ticking the other boxes.

In a sense, I could almost sum this entire post up by saying FreeBSD "feels" like the right solution for me.

### **Documentation**

The FreeBSD handbook is literally famous. People, including me, love the fact it exists and is a solid bastion of knowledge that I can go to when I need help with something. It certainly doesn't contain everything, but it contains enough to go from FreeBSD newcomer to a superstar in a few months of reading and tinkering.

On the other hand the RHEL documentation boils my blood at times. The only Linux distribution to get documentation even close to right is Arch Linux, which often comes up in Google results for various problems I have with Linux, but it's not a distro I feel comfortable running in production.

The FreeBSD Wiki is also highly maintained and even open to newcomers like me introducing more content. That content can be promoted to the Handbook if it's suitable and referenced enough.

---

All of this isn't to say Linux is a bad solution. It's really not. But I go through waves of desiring something more concrete. Something that seems slow and old, but is actually blazing fast and adopts relevant new technologies.

I'm going to try using FreeBSD going forward. I'm going to investigate Jails, The Mandatory Access Control system(s), and ZFS as the primary tools I believe can help me solve the problem of application hosting, delivery and deployment far easier than other options on the market.
