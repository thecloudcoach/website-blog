---
title: What is GitLab?
date: 2019-08-07
tags: [gitlab, ci/cd, cloudops, automation, cloud, deployment]
description: GitLab is the gold standard for Git SCM at any level within any sized organisation.
---

GitLab is a CloudOps dream-come-true. It's the only Git solution you need regardless of the team
size; it encompasses version control, continuous integration and delivery (CI/CD), project
management, and DevOps work flows; and it has a huge community as well as professional services
should you need them.

It should be used to underpin your Cloud resources and the tools used to manage them, placing it at
the heart of any Cloud adoption mandate.

### Too long; didn't read

- Git source control with all the bells and whistles
- Code reviews are a pleasure
- Merge Requests are easy to use and mange
- The issue tracker, when used with boards, are a powerful asset
- It has CI/CD right alongside your code, another powerful assets
- And the documentation is of a very high quality

Let's take a look at some of GitLab's primary features and what they do.

### Source Control

When asked about the source code for Doom and how it was managed, John Romero had the audience
laughing when he said it was all on everyone's hard drives. If you needed someone else's work to do
your own, you used a floppy disk to transfer it over to your machine. Yikes!

Source control is a means of storing code in a central location. Git is a Source Control Management
(SCM) tool. The key thing with Git is it's a distributed SCM, which means although the code can
(should) be stored in a central location it's an entirely optional – you don't actually need a
remote Git repository to use Git.

Git is becoming, or has become, the de facto SCM technology, which is GitLab's primary focus (it's
in the name!) It's definitely time to get on board if you haven't done so already.

GitLab is fast becoming the primary means by which enterprises and governments manage Git
repositories. It offers all the key features and there's little to nothing left out:

- Pull Requests
- Branches and Protected Branches
- Access Control
- Code Reviews
- Code Analytics
- Tagging
- Issues (and beyond!)

And more. Way more.

### Project Management

Most Git repository solutions allow you to create issues on a repository in order to report
problems. GitLab is certainly no different, but it also takes it a step further.

GitLab actually supports full Scrum or Kanban style project boards with swim lanes. Issues that you
create can be tagged, and the tag can be used to create swim lanes.

As a developer you can use this feature to plan and progress work through the software development
life cycle. All you have to do is use labels on issues and then create a list from said labels. You
can now progress issue cards between lists, eventually marking them as complete.

As a product owner or project manager you can get a visual indication of the work being done, the
work needed, and the work completed. It's not going to replace Jira anytime soon, but it's a viable
option for keeping low level programming tasks close to the source, leaving the big picture "Epics"
in Jira for the whole company to consume.

### CI/CD

GitLab can also replace Jenkins, BambooCI, Team City, and so on. For example instead of using
Jenkins and installing 700 plugins to get it to a usable state, use GitLab's built-in CI/CD solution
instead.

A simple YAML file in your repository is all it takes to introduce CI/CD to your product. You define
the stages and the steps for those stages. The level of flexibility and complexity is surprising
given it's all written in YAML.

Code changes can be automatically tested, linted, and accepted/rejected with Pipelines that you
define in your YAML configuration. There are some similarities between this kind of functionality
and other options like Jenkins, so the learning curve is shallow.

Once code is passing tests and reviews, you can have a Pipeline deploy new code into production, or
promote from another environment into production, all automatically. The CI/CD capabilities don't
have to just be about code – they can incorporate application deployment management too.

You can also use manual stages in Pipelines to offer one-click, automated processes for developers
and others to run. These Pipelines can be executed against code such as deployments or used to
generate artifacts such as RPMs, DEBs, Docker Images, and more.

### Kubernetes (K8s) Integration

Kubernetes is an extremely complex but powerful orchestration engine. If you've got the need for it
and you have a cluster, then the goodnews is GitLab has first-class support for K8s and also ships
with some fancy features that work exclusively with the container orchestrator.

This includes a Serverless option for running Functions as a Service/Serverless on a Kubernetes
function (currently alpha.) This is super powerful and opens up a whole world of possibilities for
running one time tasks that can clean up AMIs or power entire services.

The K8s integration also allows CI/CD workloads to be executed on a cluster. Usually with Gitlab
CI/CD you create and operate "runners" which can either run shell scripts to complete your CI/CD
tasks or use a Docker host to run Docker Images. With the K8s integration you can have GitLab run
Docker based CI/CD tasks straight on the cluster for you. For some this is reason enough to use a
(managed) K8s cluster.

A K8s cluster integrated with GitLab makes it easier to use the Auto DevOps functionality, also.
We'll go into more detail on Auto DevOps in a future post as it's a big feature.

Finally the K8s integration introduces Review Apps – the ability to instantly showcase changes made
to code from feature branches. This means a feature branch can be built and deployed to the K8s
cluster with everything else being setup to make it visible for review by the business.

### Summary

Source control is something that's absolutely required in 2019 and GitLab is the industry standard
for source control at any level. It's also the only choice when code and CloudOps are tightly
integrated, as they should be.

You can try GitLab for free online or via a self-hosted option.
