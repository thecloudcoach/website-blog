---
title: What is Packer
date: 2019-07-29
tags: [automation, aws, cloud, devops, packer]
description: Packer is the gold standard for golden images.
---

Golden images are critical for raw compute based resources to behave in a consistent, stable, secure
manner. A good golden image helps administrate large estates, dianose problems, and keep
environments minimal and clean.

In the not so distant past, I've worked on environments that used two different operating systems
for the same web servers. The web servers hosted and served the same WordPress installation, yet one
server was RHEL and another CentOS.

The tools and packages were also inconsistent, breaking and delaying Ansible automation efforts as
we had to bring the systems in alignment. 

To resolve this problem we implemented a Packer managed AMI using an official CentOS base image from
the AWS Market Place. Using Ansible, which was executed by Packer, to install a few key packages and
implement some security standards, we had a solid foundation on which to build future web servers.

Packer is the key to creating and managing golden images. It's the **golden** (heh) standard, in
fact. When it comes to managing AWS AMIs, nothing beats it.

### Too long; didn't read

At the end of the day you'll need solid AMIs for security, patching, and stable deployments. If
servers all us different Linux distributions and or packages, automation will become a complete
headache.

Packer can manage your AMIs with ease. It's made that very job. It's also able to reuse your Ansible
code as a way to provision images for faster deployments.

It's also a relatively small tool, so getting up and running with it is quick and easy. Using some
example code and a bit of reading, you can have a new AMI ready within a day.

And it's yet more "As Code". Golden Images As Code, you could say (GIAC?) That means we're in a
position to peer review changes to images and also have images generated automatically on a
schedule.

So what are Packer's key features? If you're interested to know a bit more, then below we take a
look at some of the things it has to offer you.

### Configuration

Packer's configuration is all done in simple JSON files. Although JSON isn't the most readable or
writable of formats, it's actually not overly nested or deep, so it's very simple to read and
develop Packer managed images.

And you can combine things into a single JSON file or break them out into multiple files. That is to
say if you want to build multiple images with a single innvocation of Packer, you can. You're going
to have a better if you keep things spearate, however.

Packer's configuration includes three key components:

1. Builders
2. Provisioners
3. Post Processors

### Builders

Builders are what talk to a remote Cloud provider and build out the resource (EC2 Instance) needed
to make an image. So in the case of AWS, an EC2 Instance to create an AMI.

Packer supports many builders, from AWS, GCP and Azure, to Alibaba and many other providers. There
are close to 50 builders at the time of writing.

The process a Builder goes through is simple:

1. Build an EC2 Instance (for example)
2. Provision it with Ansible
3. Shut down the EC2 Instance
4. Create an AMI
5. Delete the EC2 Instance
6. Tell you the AMI ID

Then you use the AMI in Terraform code sets to build new systems in a consistent fashion. 

### Provisioners

Provisioners are were a lot of the magic happens. Using Ansible, Packer can provision the remote EC2
Instance with your desired configuration before shutting it down and creating an AMI from it.

Being able to run Ansible is powerful because it means no shell scripts are needed. You simply reuse
your existing Ansible code base and take advantage of the stability and speed it offers.

Also having a single Ansible code base and pulling it alongside Packer when building is easily done
with Git Submodules. That is to say you have your Packer code in a repository and use a Submodule to
pull down your Ansible code to a sub-directory. Packer can then use that Ansiblre code base to
provision systems.

But Ansible isn't the only option (but it is the better of them.) There are loads of provisioning
options are available from simple shell scripts to Puppet and Chef.

And multiple provisioners can be used on the same image. For example it's common to see a pattern of
installing Ansible on the remote EC2 Instance using a shell script, and then using an Ansible
provisioner (executed remotely) to do the rest of the job.

### Post Processor

Post Processors do stuff after the image has been created. You can do clean up operations, publish
the AMI ID somewhere, etc. It's an optional step.

There's not much to be said on Post Processors. They're a pretty obvious addition to Packer's
functionality.

### Summary

If you like having a consistent, secure infrastructure then you need Golen Images.

Packer makes it very easy to build and manage AMIs on a continuous basis. The fact it can
incorporate your existing Ansible code helps keeps things simple yet powerful.

If you need help implementing Packer, please feel free to get in touch.
