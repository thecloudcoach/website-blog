---
title: Terraform Crash Course
description: I made a video course for anyone looking to get upto speed quickly with Terraform.
date: 2019-11-13
tags: [terraform, iac, aws, automation]
---

I recently created a video training course called "[Terraform Crash
Course](https://www.thecloud.coach/terraform-crash-course)". It's designed for those of you who are
new to the concepts of Infrastructure As Code and Terraform.

In [the course](https://www.thecloud.coach/terraform-crash-course) I take you through the very
basics of standing up some AWS infrastructure whilst talking to a few basic concepts you'll need to
understand to get started with Terraform. **[Check it
out!](https://www.thecloud.coach/terraform-crash-course)**

It's hosted on a platform called Podia. All that is required is your **name** and **email** address,
but if this isn't something you're willing to give up then **fake details will work** just as well
and get you to the content.

Please feel free to reach out with any questions, concerns, feedback or requests.

