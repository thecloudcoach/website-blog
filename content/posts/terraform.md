---
title: What is Terraform?
date: 2019-07-27
tags: [terraform, automation, aws, cloud, devops, iac]
description: Terraform is Infrastructure As Code. It's the industry standard. It's the present and future of managing Cloud environments at any scale.
---

Before Terraform, engineers would...

1. Login to the AWS Console and start provisioning resources manually
2. Start linking those resources together
3. Accidentally provision a 3TB EBS Volume instead of a 300GB EBS Volume, costing the business $300
   instead of $30
4. Try and then replicate all of that infrastructure again into three new environments

Eventually someone would have to manage this estate. Changes would come in manually, unbeknown to
the rest of the team (at least until something broke) and would never be documented.

It became a complete mess very quickly.

Terraform provides the Infrastructure As Code (IAC) and enables engineers to completely disconnect
themselves from web consoles and APIs, abstracting away the management of Cloud providers with code.
Now engineers...

1. Open up their editor of choice
2. Write out some HCL to describe the infrastructure they want
3. Push the code to GitLab and request a code review via a Merge Request
4. Eventually apply the code to their AWS account and watch as the resources are all created for
   them

Now they have peer reviewed, tested code; an audit trial, code that's acting as documentation; and
plenty of time on their hands.

With its simple HashiCorp Configuration Lanaguage (HCL) and easy CLI interface, paired with a
dependency graph, Terraform changed everything for OpsFactory and CloudOps as we know it.

It can do the same for you too.

## Too long; didn't read

Managing infrastructure as code is key to being able to scale quickly. And speed is important. Being
able to get an idea to market and validate it quickly is a huge money saving goal for most new
businesses. Being able to quickly pivot and scale an existing service to meet demand is how you
survive.

Terraform is quick to learn, quick to implement, and easy to maintain from that point forward. There
are also commercial services and products around Terraform, allowing all organisations of all types
and sizes to adopt Terraform.

Terraform is also **the industry standard** for implementing and managing Infrastructure As Code. It
doesn't matter if you have a single AWS account with a few EC2 instances or you have an IRAP
compliant estate spread across sixteen AWS accounts, Terraform will scale with you.

It's also key to implementing and managing a multi-Cloud deployment, a requirement that's becoming
more and more popular in both the public and private sectors. Terraform supports close to 50 Cloud
providers at the time of writing, including all the key players such as AWS, GCP, Azure, and more.

You get a very flexible tool, too. Writing modules and providers allows for extreme amounts of
flexbility and code reuse. State you're defining for one part of your business can be adopted in
another part, with 90% of the work done already.

And Terraform is perfectly designed for the job it's advertised to do. It doesn't try to be
everything to everyone: it simply builds and manages infrastructure. You want your tools to have a
singular responsibility when it comes to your infrastructure.

Let's take a high level look at some of Terraform's key features.

### The DAG (Directional Acyclic Graph)

Terraform's dependency graph is all about understanding the (side) effects of your actions ahead of
time. This is one of Terraform's killer features and it's very powerful.

What is means for you is simple: when a change is made to code, who knows what that change will
impact on a larger scale? For example an engineer can make a change to an EC2 Instance that causes
AWS to destroy and rebuild the instance. The engineer might not realise that's a side effect of
their actions, and as such, it causes downtime for end users.

With Terraform, another engineer can not only review the code its self and try to determine the
outcome, but they can review a "plan" that Terraform generates for them removing the need for such
risky, error prone guess work. Instead, the engineer uses this plan to point out that the change is
destructive and instead of doing it now, it should be scheduled as an out of hours task.

That's insanely powerful for your business.

### HCL

HashiCorp Configuration Language (HCL), the language in which you define state, is specifically
designed for the job of declaratively defining infrastructure. And because it's purpose built it
means there are certain ways you do certain things. There's one way you define an EC2 Instance, not
one hundred.

And HCL's syntax is small. There aren't a lot of key words or complex tricks to understand. It can
be picked up in a weekend by anyone with basic programming skills, and perhaps a bit more time for
those completely new to programming.

### Flexibility Built In

Terraform's is modular in design and thus supports well over 30 Cloud providers, out of the box, at
the time of writing. It's also relatively easy to develop new providers for custom systems, but for
99% of users this won't be required.

Terraform also supports and encourages code reusability through the use of modules. When you have a
collection of resources all coming together to deliver a solution, which is then repeatedly used
elsewhere, a module can be used to eliminate code repetition. Modules can also be stored in Git and
version controlled for added security and stability.

### Extending Terraform

Another trick Terraform can do is execute a binary to collect external facts from anywhere you can
imagine (and write code for.) So instead of having facts defined in a flat `.tfvars` in the same Git
repository, you might instead want to store them in a PostgreSQL DB (and use a web UI to interface
with that DB.) A custom binary, outputting the correct JSON document, can be used to pull that data
for you.

### The State File

Speaking of flat files, Terraform's `.tfstate` file is a flat JSON file, enabling some pretty
powerful opportunities, such as using it as a dynamic Ansible inventory source.

And finally it supports pushing that statefile to remote storage such as S3 as well as using a
database to lock statefile access during executions. A popular combination is S3 (with versioning
enabled) and DynamoDB for state locking.

### Summary

Terraform literally is Infrastructure As Code. It's the industry standard of IAC. It's the only tool
you should consider, if we may be so bold.

Everything from it's dependency graph to it's simple configuration language, easy to use command
line interface, highly customisable nature, and professional services makes Terraform suitable for
all workloads of all sizes.

Terraform is a powerhouse. If you're not using it already, you should be. If you don't know where to
start, [get in touch today.](https://opsfactory.com.au/contact)
