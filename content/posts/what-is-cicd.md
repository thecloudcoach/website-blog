---
title: What is CI/CD?
date: 2019-08-02
tags: [ci/cd, automation, cloud, devops, testing, deployment]
description: A solid CI/CD stack can be the deciding factor between you and your primary competition - who can deploy features faster and better?
---

Continuous Integration and Continuous Delivery (CI/CD) are two separate but often tightly bound
concepts.

Continuous Integration (CI) can be viewed as a process of automatically integrating tools and source
code. In the case of Terraform, for example, that means integrating Terratest and linting checks
against Terraform code to ensure they stand up to business requirements. Once this work is done, we
move onto delivering it.

Continuous Delivery (CD) is the next natural step in the pipeline. It's the act of taking code
that's been reviewed (automatically or otherwise) and delivering it through automated pipelines.
OpsKit does this for Terraform by having a planning stage followed by a manual apply stage.

These two processes combined result in a continuous cycle of pushing code, testing it, and then
delivering it. 

### Testing

When we develop software we put ideas into code. Those ideas aren't always translated correctly and
they can break prior works. One method of ensuring new code don't break existing, working solutions
is to test it.

Testing comes in many different forms. We can test individual units of code (functions), modules,
systems or test the integration of components between one another.  Regardless of the methodologies
used, CI is where testing should be done.

Having code tested the moment it's pushed, as opposed to assuming the developer has done the testing
themselves, drastically reduces regressions, human error and performance degradation when deployed.

CI is also gate keeper as well as a way of automating tests for convenience. The business can define
their expectations of code, as code, enabling a CI tool to continuously test if the expectaitons are
being met, such as performance enhancements or security assessments.

That's why testing is powerful, but it's even more powerful when we automate it and tightly
integrate it into the process of developing new ideas.

### Continuous Deployments

Software needs to be deployed, otherwise writing software would be (somewhat) pointless as people
would never get to use it. It is therefore a good question for any business to ask its self is, "How
are we deploying our software?"

The process of making software accessible to millions of people is complex. It depends how the
software has been written, the platform it targets, etc. Tools and services are coming and going all
the time which assist us with this endeavour, and all of them become more powerful when integrated
into a CD process.

Continuous Delivery will push software into an environment in a manner that removes human error
entirely. CD is about save repeatability. Being able to successfully and automatically deploy
software after it has been written saves time and money.

But CD is also about protecting against faulty code and rolling back to a previous version. That is
to say that CD goes in both directions. A solid CD implementation will automatically push new ideas
into production after automated testing validates the ideas as suitable, but it can also push
previously working code into play if the new idea failed.

This is once again it's about raising confidence in your processes which your customers will see in
the quality of your products and services.

### Summary

A good CI/CD stack doesn't have to be overly complicated or hard to get in place. You don't need a
team of people designing it, implementing it and managing it. It can be as simple as making sure
tests are automatically executed and that new ideas are pushed to a test environment or packaged
into an artifact, such as a Docker image.

If you need assistance with your CI/CD work flows please feel free to get in touch.
