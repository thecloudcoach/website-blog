---
title: What is CloudOps?
date: 2020-02-07T19:23:21+10:00
draft: true
description: Could CloudOps just be another new buzzword or does it have some sort of substance?
tags: ["cloudops", "devops", "cloud"]
---

Unlike DevOps, the term CloudOps is used to describe something more. When we talk about DevOps what
we're discussing are our processes and methodologies for doing certain things in a certain way.
Namely how we integrate development teams with operational teams to ensure there's a frictionless,
automated transition between what the developers develop and what operations deploy and maintain.

CloudOps is very much like DevOps but with specific technologies in mind. 

## DevOps vs CloudOps

Someone who has the title of "CloudOp" isn't working outside of public Cloud providers. Everything
they're hosting is being done inside of a public Cloud provider such as AWS, GCP or Azure. It could
be said that the title gives this away, which is the point entirely: "CloudOps" speaks directly to
what we're using and how we're using it.

Unlike the term "DevOp", which isn't a job title to begin with, "CloudOps" actually infers the stack
being used by the person. We can determine that someone who calls themselves a "CloudOp" is working
with public Cloud technologies and is very likely to be using a specific set of tools to automate
the delivery of those Cloud solutions. Tools like Terraform, Ansible, GitLab, Kubernetes, and more.

Someone who says they do "DevOps" is saying: we've broken down the barriers between
our developers and operations staff to enable us to automate software delivery more smoothly. But
what technologies are being used? They could be anything and the possibilities are endless. 

That's why CloudOps is both a job title and a set of methodologies: we're using DevOps to delivery
software to public Cloud resources using Cloud-first tools.

## Tools for the Cloud

Although tools like Terraform can be used with on-premise solutions they are, in fact, Cloud first
tools. They're designed to work with the APIs offered by public Cloud providers to abstract the
complexities and make it easier to manage them. Such tools make little work of doing the same for
non-Cloud technologies but in fact the support for public Cloud tends to be much, much stronger
than it does for non-Cloud solutions.

