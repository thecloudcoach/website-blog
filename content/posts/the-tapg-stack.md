---
title: What is OpsKit?
slug: the-tapg-stack
date_published: 1970-01-01T00:00:00.000Z
date_updated: 2019-07-19T07:30:13.000Z
draft: true
---

OpsKit is an OpsFactory offering that takes away the burden of managing four key technologies:

- Terraform
- Ansible
- Packer
- GitLab (CI/CD)

OpsKit also encompasses managed Terraform and Ansible code libraries, as well as processes for delivering bespoke infrastructure solutions.

It's also a knowledge base providing training materials that clients can use to educate themselves on the inner workings of the technologies involved, enabling them to better self-serve and even develop their own solution (we're not here to lock you in.)

In this article we want to discuss the four key technologies we use daily through OpsKit to better understand their role within the OpsKit stack.You'll also gain insights into why they're able to solve so many problems for so many people, time and time again.
